﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRageMath;

namespace IngameScript
{
  partial class Program
  {
    public class AmmoStatus
    {
      private Dictionary<IMyLargeTurretBase, string> _turrets = new Dictionary<IMyLargeTurretBase, string>();
      private IMyCargoContainer _mainContainer;

      public AmmoStatus(Program program, UpdateType updateSource)
      {
        List<IMyLargeTurretBase> tempTurrets = new List<IMyLargeTurretBase>();
        program.GridTerminalSystem.GetBlockGroupWithName("TurretBunker.Turrets").GetBlocksOfType(tempTurrets);
        LoadTurretsWithAmmoTypes(program, tempTurrets);
        if (updateSource == UpdateType.Terminal)
        {
          SaveAmmoTypesToStorage(program);
        }

        _mainContainer = program.GridTerminalSystem.GetBlockWithName("TurretBunker.MainAmmoContainer") as IMyCargoContainer;
      }

      public string[] GetLowAmmoNames()
      {
        if (_turrets == null)
          throw new Exception("No turret group set");

        return _turrets.Where(x => x.Key.GetInventory().CurrentVolume.RawValue < x.Key.GetInventory().MaxVolume.RawValue / 20)
          .Select(x => x.Value).ToArray();

      }

      public bool LowStorage(string ammoType)
      {
        if (_mainContainer == null)
          throw new Exception("No main storage set");

        return !_mainContainer.GetInventory().GetItems().Any(x => x.Content.SubtypeName == ammoType);
      }

      private void SaveAmmoTypesToStorage(Program program)
      {
        if (_turrets == null)
          throw new Exception("No turret group set");

        string temp = "";
        foreach (KeyValuePair<IMyLargeTurretBase, string> turret in _turrets)
        {
          temp += turret.Key.EntityId.ToString() + "-" + turret.Value + ";";
        }
        program.Storage = temp;
      }

      private void LoadTurretsWithAmmoTypes(Program program, List<IMyLargeTurretBase> turrets)
      {
        if (_turrets == null)
          throw new Exception("No turret group set");

        foreach (IMyLargeTurretBase turret in turrets)
        {
          IMyInventoryItem ammoItem = turret.GetInventory().GetItems().FirstOrDefault();
          if (ammoItem != null)
          {
            _turrets.Add(turret, ammoItem.Content.SubtypeName);
          }
          else
          {
            string backupType = program.Storage.Split(';').Where(x => x.Contains(turret.EntityId.ToString() + "-")).FirstOrDefault();
            if (!String.IsNullOrEmpty(backupType))
            {
              _turrets.Add(turret, backupType.Split('-')[1]);
            }
            else
              throw new Exception("For the correct setup run first time from console with every turret containing some ammo!");
          }
        }
      }
    }
  }
}
